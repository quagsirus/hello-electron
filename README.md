# hello-electron
hello world, written in TypeScript and electron.js
## usage guide
```bash
# Install dependencies
npm install
# Compile the typescript
npm run build
# Start the electron app
npm start
```