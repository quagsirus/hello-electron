import { app, BrowserWindow } from 'electron';

function createWindow() {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    height: 600,
    width: 800,
    backgroundColor: '#fff',
    webPreferences: {
      nodeIntegration: true,
    }
  });
  // Remove chromium menubar
  mainWindow.removeMenu();
  // Loads the index of the app
  mainWindow.loadFile('index.html');
}

// Creates main window after app loaded
app.on('ready', () => {
  createWindow();

  app.on('activate', function () {
    // Adds better MacOS dock compatibility
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

// Accounts for MacOS standard app behaviour
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});